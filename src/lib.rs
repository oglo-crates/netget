use std::io;
use std::io::Write;
use std::fs;
use std::cmp::min;
use reqwest::Client;
use indicatif::{ProgressBar, ProgressStyle};
use futures_util::StreamExt;
use url::Url;
use tokio::runtime::Runtime;

/// The config for how to style the progress bar
/// Note: if the config is invalid, the bar will use the default style
pub struct ProgressBarConfig {
    pub bar: String,
    pub message: String,
    pub chars: String,
}

impl ProgressBarConfig {
    pub fn default() -> Self {
        return Self {
            bar: String::from("{msg} [{wide_bar}] [{elapsed_precise}] {bytes}/{total_bytes} ({bytes_per_sec}, {eta})"),
            chars: String::from("=> "),
            message: String::from("Download ${filename}"),
        };
    }
}

/// Download a file to a directory
/// Arguments: (url to the file, directory to put the file in)
///
/// # Examples:
/// ```rust,ignore
/// fn main() {
///     let url = "https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-desktop-amd64.iso";
///     let destination = std::env::current_dir().unwrap().display().to_string(); // The destination path for the file.
///
///     netget::download_file(url, &destination, &netget::ProgressBarConfig::default()).unwrap();
/// }
/// ```
pub fn download_file<P: AsRef<str> + std::fmt::Display>(url: &str, drop_off_path: P, config: &ProgressBarConfig) -> Result<(), io::Error> {
    let runtime = Runtime::new()?;

    return runtime.block_on(download_file_async(url, drop_off_path, config));
}

async fn download_file_async<P: AsRef<str> + std::fmt::Display>(url: &str, drop_off_path: P, config: &ProgressBarConfig) -> Result<(), io::Error> {
    let res = Client::new().get(url).send().await.or(Err(io::Error::new(io::ErrorKind::Other, "Failed to get from url!")));

    let res = res?;

    let total_size = match res.content_length() {
        Some(s) => s,
        None => return Err(io::Error::new(io::ErrorKind::Other, "Failed to get content length from url!")),
    };

    let file_name = match get_file_name_from_url(url) {
        Some(s) => s,
        None => return Err(io::Error::new(io::ErrorKind::Other, "Failed to get file name from url!")),
    };

    let pb = ProgressBar::new(total_size);

    let style_pre = match ProgressStyle::default_bar().template(&config.bar) {
        Ok(o) => o,
        Err(_) => ProgressStyle::default_bar().template(&ProgressBarConfig::default().bar).unwrap(),
    };

    pb.set_style(style_pre.progress_chars(&config.chars));

    let message = config.message
        .replace("${filename}", &file_name)
        .replace("${url}", url);

    pb.set_message(message.clone());

    let fpath: String = format!("{}/{}", drop_off_path, file_name);

    let mut file = fs::File::create(fpath)?;
    let mut downloaded: u64 = 0;
    let mut stream = res.bytes_stream();

    while let Some(item) = stream.next().await {
        let chunk = item.or(Err(io::Error::new(io::ErrorKind::Other, "Error while downloading file!")))?;
        file.write_all(&chunk)?;
        let new = min(downloaded + (chunk.len() as u64), total_size);
        downloaded = new;
        pb.set_position(new);
    }

    pb.finish_with_message(message);

    return Ok(());
}

pub fn get_file_name_from_url(url: &str) -> Option<String> {
    let parsed_url = Url::parse(url).ok()?;
    let segments: Vec<&str> = parsed_url.path_segments()?.collect();

    return segments.last().map(|s| s.to_string());
}
