# NetGet

NetGet is a library for downloading files from the internet, it also supports styling the progress bar!



# Examples:

```rust
fn main() {
    let url = "https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-desktop-amd64.iso";
    let destination = std::env::current_dir().unwrap().display().to_string(); // The destination path for the file.

    netget::download_file(url, &destination, &netget::ProgressBarConfig::default()).unwrap();
}
```
